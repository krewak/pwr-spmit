$(document).ready(function() {

	$("#logout").click(function() {
		$("#logout-form").submit();
	});

	$(".change-current-brewery").click(function() {
		$(this).submit();
	});

	$(".dashboard-tile.add-new").click(function() {
		location.href = $(this).data("url");
	});

	$(".planned-day").hover(function() {
		var batch = $(this).data("batch-id");
		if(batch) {
			$(".planned-day:not([data-batch-id=" + batch + "])").addClass("blur");

			$(".fermenter").each(function() {
				if(!$(this).children(".planned-day[data-batch-id=" + batch + "]").length) {
					$(this).hide();
				}
				
				$(this).children(".planned-day[data-batch-id=" + batch + "]:first").tooltip("show");
				$(this).children(".planned-day[data-batch-id=" + batch + "]:last").tooltip("show");
			});
		}

	}, function() {
		$(".planned-day").removeClass("blur").tooltip("hide");
		$(".fermenter").show();
	});

	$("select[name='style_id']").change(function() {
		var $selected_option = $(this).find(":selected");
		var blg = (parseFloat($selected_option.data("min-blg")) + parseFloat($selected_option.data("max-blg"))) / 2;

		var days;

		if(blg < 8) {
			days = [7, 7];
		} else if(blg < 12) {
			days = [8, 7];
		} else if(blg < 16) {
			days = [9, 12];
		} else if(blg < 19) {
			days = [12, 12];
		} else if(blg < 22) {
			days = [14, 16];
		} else {
			days = [18, 22];
		}

		$("input[name='primary_days']").val(days[0]);
		$("input[name='secondary_days']").val(days[1]);
	});

	function errorCase() {
		console.log("File not found, wait 2.5 seconds...");
		setTimeout(function() {
			checkIfOutFileHasBeenGenerated();
		}, 2500);
	}

	function checkIfOutFileHasBeenGenerated() {
		$.ajax({
			url: $("#preloader").data("checkfile"),
			type: "post",
			data: {
				"_token": $("#_token").val(),
				"batch_id": $("#preloader").data("batch-id"),
			},	
			dataType: "json",
			success: function(e) {
				if(e.success) {
					window.location.href = e.url;
				} else {
					errorCase();
				}
			},
			error: function() {
				errorCase();
			}
		});
	}

	if($("#preloader").length) {
		checkIfOutFileHasBeenGenerated();
	}

});