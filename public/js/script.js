$(document).ready(function() {

	$(".alert-dismissible").fadeOut(5000, function() {
		$(this).css({"visibility": "hidden", display: "block"}).slideUp();
	});

	$(".alert-dismissible").hover(function() {
		$(this).stop();
		$(this).fadeIn(100);
	});

	$(".alert-dismissible").mouseout(function() {
		$(this).stop();
		$(this).fadeOut(5000, function() {
			$(this).css({"visibility": "hidden", display: "block"}).slideUp();
		});
	});

	$("[data-toggle='tooltip']").tooltip();

});