clear all;
close all;
% [a,b] = optymalizuj([2,2,2,2]);
ObjectiveFunction = @optymalizuj;
X0 = [1 1 1 1];   % Starting point
% [x,fval,exitFlag,output] = simulannealbnd(ObjectiveFunction,X0);
[fermentatory, cel] = readFile();
lb = [1,1,1,1];
ub = [365-cel.czas1-cel.czas2, 6, 365-cel.czas2, 6];
% opts = optimoptions('ga','PlotFcn',@gaplotbestf);
% rng(1,'twister') % for reproducibility
IntCon = [1,2,3,4];
[x,fval,exitflag] = ga(@optymalizuj,4,[],[],[],[],...
    lb,ub,[],IntCon)
[fermentatory, cmax] = warzenie(x);
saveFile(fermentatory, cmax);