function [fermentatory, cel] = readFile()
fileID = fopen('C:\wamp\www\pwr\spmit\storage\app\in.txt','r');
tline = fgetl(fileID);
czas1 = 0;
czas2 = 0;
objetoscWarki = 0;
if ischar(tline)
    czas1 = str2double(tline);
end
tline = fgetl(fileID);
if ischar(tline)
    czas2 = str2double(tline);
end
tline = fgetl(fileID);
tline = fgetl(fileID);
if ischar(tline)
    objetoscWarki = str2double(tline);
end
cel = struct('czas1',czas1,'czas2',czas2,'objetoscWarki',objetoscWarki);
tline = fgetl(fileID);
fermentatory = [];
i = 1;
while ischar(tline)
    tline = fgetl(fileID);
    if ischar(tline)
        fermentatory(i).pojemnosc= str2double(tline);
    end
    tline = fgetl(fileID);
    if ischar(tline)
        fermentatory(i).zasoby = tline-'0';
    end
    i=i+1;
end
fclose(fileID);
end