function [fermentatoryWyj, cmax]= optymalizuj(arg)
[fermentatory, cel] = readFile();
czas1 = cel.czas1;
czas2 = cel.czas2;
pocz1 = arg(1);
pocz2 = arg(3);
ktory1 = arg(2);
ktory2 = arg(4);
fermentatoryWyj = fermentatory;
%

if floor(ktory1) == ktory1 && floor(ktory2) == ktory2 ...
        && floor(pocz1) == pocz1 && floor(pocz2) == pocz2
    if length(fermentatoryWyj) < ktory1 || length(fermentatoryWyj) < ktory2...
           || pocz1 > 365 || pocz2 > 365 || pocz1+czas1 > pocz2 ...
           || ktory1 == ktory2
       cmax = realmax('single');
       return;
    end
    if 0 > ktory1 || 0 > ktory2 || pocz1 < 0 || pocz2 < 0
       cmax = realmax('single');
       return;
    end
    % sprawdzenie pojemnosci
    if fermentatoryWyj(ktory1).pojemnosc < cel.objetoscWarki ...
            || fermentatoryWyj(ktory2).pojemnosc < cel.objetoscWarki
        cmax = realmax('single');
       return;
    end
    % zajecie dla pierwszego etapu
    for i = pocz1:pocz1+czas1-1
        if fermentatoryWyj(ktory1).zasoby(i) ~= 0
            cmax = realmax('single');
            return;
        end
        fermentatoryWyj(ktory1).zasoby(i) = 2;
        tmp1 = i;
    end
    % zajecie dla drugiego etapu
    for i = pocz2:pocz2+czas2-1
        if fermentatoryWyj(ktory2).zasoby(i) ~= 0
            cmax = realmax('single');
            return;
        end
        fermentatoryWyj(ktory2).zasoby(i) = 3;
        tmp2 = i;
    end
    %podstawienie cmax
    if tmp1 > tmp2
        cmax = tmp1;
    else
        cmax = tmp2;
    end
    
else
    cmax = realmax('single');
    return;
end

end