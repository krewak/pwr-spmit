<?php

use Illuminate\Database\Seeder;
use App\Models\Fermenter;

class FermenterSeeder extends Seeder {

	public function run() {
		$fermenter = Fermenter::firstOrCreate(["id" => 1]);
		$fermenter->name = "FM-1";
		$fermenter->capacity = 33;
		$fermenter->brewery_id = 1;
		$fermenter->save();

		$fermenter = Fermenter::firstOrCreate(["id" => 2]);
		$fermenter->name = "FM-2";
		$fermenter->capacity = 33;
		$fermenter->brewery_id = 1;
		$fermenter->save();

		$fermenter = Fermenter::firstOrCreate(["id" => 3]);
		$fermenter->name = "FM-3";
		$fermenter->capacity = 33;
		$fermenter->brewery_id = 1;
		$fermenter->save();
	}

}
