<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use \Hash;

class UserSeeder extends Seeder {

	public function run() {
		$user = User::firstOrCreate(["id" => 1]);
		$user->name = "krewak";
		$user->email = "krewak@spmit.pwr";
		$user->password = Hash::make("krzysztof");
		$user->is_admin = true;
		$user->save();

		$user = User::firstOrCreate(["id" => 2]);
		$user->name = "mbalogh";
		$user->email = "mbalogh@spmit.pwr";
		$user->password = Hash::make("mihaly");
		$user->is_admin = true;
		$user->save();

		$user = User::firstOrCreate(["id" => 3]);
		$user->name = "jpempera";
		$user->email = "jpempera@spmit.pwr";
		$user->password = Hash::make("jaroslaw");
		$user->is_admin = true;
		$user->save();
	}

}
