<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	public function run() {
		$this->call("UserSeeder");
		$this->call("BeerStyleSeeder");
		$this->call("BrewerySeeder");
		$this->call("VatSeeder");
		$this->call("FermenterSeeder");
		$this->call("BatchSeeder");
	}

}
