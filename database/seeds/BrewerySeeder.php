<?php

use Illuminate\Database\Seeder;
use App\Models\Brewery;

class BrewerySeeder extends Seeder {

	public function run() {
		$brewery = Brewery::firstOrCreate(["id" => 1]);
		$brewery->name = "Browar domowy Aurora";
		$brewery->user_id = 1;
		$brewery->save();
	}

}
