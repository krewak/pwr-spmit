<?php

use Illuminate\Database\Seeder;
use App\Models\Vat;

class VatSeeder extends Seeder {

	public function run() {
		$vat = Vat::firstOrCreate(["id" => 1]);
		$vat->name = "KZW-1";
		$vat->capacity = 40;
		$vat->brewery_id = 1;
		$vat->save();
	}

}
