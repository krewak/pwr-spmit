<?php

use Illuminate\Database\Seeder;
use App\Models\Batch;

class BatchSeeder extends Seeder {

	public function run() {
		$batch = Batch::firstOrCreate(["id" => 1]);
		$batch->name = "Warka testowa 001";
		$batch->brewery_id = 1;
		$batch->style_id = 96;
		$batch->volume = 25;
		$batch->brewing_date = "2016-06-08";
		$batch->primary_days = 7;
		$batch->secondary_days = 10;
		$batch->primary_fermenter_id = 1;
		$batch->secondary_fermenter_id = 2;
		$batch->save();

		$batch = Batch::firstOrCreate(["id" => 2]);
		$batch->name = "Warka testowa 002";
		$batch->brewery_id = 1;
		$batch->style_id = 83;
		$batch->volume = 26;
		$batch->brewing_date = "2016-06-10";
		$batch->primary_days = 8;
		$batch->secondary_days = 12;
		$batch->primary_fermenter_id = 3;
		$batch->secondary_fermenter_id = 1;
		$batch->save();

		$batch = Batch::firstOrCreate(["id" => 3]);
		$batch->name = "Warka testowa 003";
		$batch->brewery_id = 1;
		$batch->style_id = 65;
		$batch->volume = 28;
		$batch->brewing_date = "2016-06-19";
		$batch->primary_days = 11;
		$batch->secondary_days = 16;
		$batch->primary_fermenter_id = 3;
		$batch->secondary_fermenter_id = 1;
		$batch->save();

		$batch = Batch::firstOrCreate(["id" => 4]);
		$batch->name = "Warka testowa 004";
		$batch->brewery_id = 1;
		$batch->style_id = 96;
		$batch->volume = 28;
		$batch->brewing_date = "2016-06-26";
		$batch->primary_days = 7;
		$batch->secondary_days = 10;
		$batch->primary_fermenter_id = 2;
		$batch->secondary_fermenter_id = 3;
		$batch->save();
	}

}
