<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeerStylesTable extends Migration {

	public function up() {
		Schema::create("beer_styles", function (Blueprint $table) {
			$table->increments("id");

			$table->string("symbol");
			$table->string("name");

			$table->double("abv_min", 3, 1)->nullable()->default(null);
			$table->double("abv_max", 3, 1)->nullable()->default(null);
			$table->double("og_min", 4, 3)->nullable()->default(null);
			$table->double("og_max", 4, 3)->nullable()->default(null);
			$table->double("fg_min", 4, 3)->nullable()->default(null);
			$table->double("fg_max", 4, 3)->nullable()->default(null);
			$table->integer("ibu_min")->nullable()->default(null);
			$table->integer("ibu_max")->nullable()->default(null);
			$table->integer("srm_min")->nullable()->default(null);
			$table->integer("srm_max")->nullable()->default(null);
		});
	}

	public function down() {
		Schema::drop("beer_styles");
	}

}
