<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentBreweryIdColumnToUsersTable extends Migration {

    public function up() {
        Schema::table("users", function(Blueprint $table) {
            $table->integer("current_brewery_id")->unsigned()->nullable()->default(null);
            $table->foreign("current_brewery_id")->references("id")->on("breweries")->onDelete("set NULL");
        });
    }

    public function down() {
        Schema::table("users", function(Blueprint $table) {
            $table->dropForeign("users_current_brewery_id_foreign");
            $table->dropColumn("current_brewery_id");
        });
    }

}
