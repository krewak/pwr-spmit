<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFermentersTable extends Migration {

	public function up() {
		Schema::create("fermenters", function (Blueprint $table) {
			$table->increments("id");
			$table->string("name");
			$table->integer("capacity");
			$table->integer("brewery_id")->unsigned()->nullable();
			$table->foreign("brewery_id")->references("id")->on("breweries")->onDelete("cascade");
		});
	}

	public function down() {
		Schema::table("fermenters", function(Blueprint $table) {
			$table->dropForeign("fermenters_brewery_id_foreign");
			$table->dropColumn("brewery_id");
		});

		Schema::drop("fermenters");
	}

}
