<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBreweriesTable extends Migration {

	public function up() {
		Schema::create("breweries", function (Blueprint $table) {
			$table->increments("id");
			$table->string("name");
			$table->integer("user_id")->unsigned()->nullable();
			$table->foreign("user_id")->references("id")->on("users");
			$table->timestamps();
		});
	}

	public function down() {
		Schema::table("breweries", function(Blueprint $table) {
			$table->dropForeign("breweries_user_id_foreign");
			$table->dropColumn("user_id");
		});

		Schema::drop("breweries");
	}

}
