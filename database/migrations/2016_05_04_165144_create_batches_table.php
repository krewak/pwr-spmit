<?php

use App\Models\Batch;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchesTable extends Migration {

	public function up() {
		Schema::create("batches", function (Blueprint $table) {
			$table->increments("id");
			$table->string("name");

			$table->integer("brewery_id")->unsigned()->nullable();
			$table->foreign("brewery_id")->references("id")->on("breweries");

			$table->integer("style_id")->unsigned()->nullable();
			$table->foreign("style_id")->references("id")->on("beer_styles");

			$table->integer("volume");

			$table->integer("status")->unsigned()->default(Batch::PLANNED);

			$table->date("brewing_date");
			$table->integer("primary_days");
			$table->integer("secondary_days");

			$table->integer("primary_fermenter_id")->unsigned()->nullable();
			$table->foreign("primary_fermenter_id")->references("id")->on("fermenters");

			$table->integer("secondary_fermenter_id")->unsigned()->nullable();
			$table->foreign("secondary_fermenter_id")->references("id")->on("fermenters");
		});
	}

	public function down() {
		Schema::table("batches", function(Blueprint $table) {
			$table->dropForeign("batches_brewery_id_foreign");
			$table->dropColumn("brewery_id");
		});

		Schema::table("batches", function(Blueprint $table) {
			$table->dropForeign("batches_style_id_foreign");
			$table->dropColumn("style_id");
		});

		Schema::table("batches", function(Blueprint $table) {
			$table->dropForeign("batches_primary_fermenter_id_foreign");
			$table->dropColumn("primary_fermenter_id");
		});

		Schema::table("batches", function(Blueprint $table) {
			$table->dropForeign("batches_secondary_fermenter_id_foreign");
			$table->dropColumn("secondary_fermenter_id");
		});

		Schema::drop("batches");
	}

}
