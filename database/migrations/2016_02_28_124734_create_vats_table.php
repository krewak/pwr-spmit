<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVatsTable extends Migration {

	public function up() {
		Schema::create("vats", function (Blueprint $table) {
			$table->increments("id");
			$table->string("name");
			$table->integer("capacity");
			$table->integer("brewery_id")->unsigned()->nullable();
			$table->foreign("brewery_id")->references("id")->on("breweries")->onDelete("cascade");
		});
	}

	public function down() {
		Schema::table("vats", function(Blueprint $table) {
			$table->dropForeign("vats_brewery_id_foreign");
			$table->dropColumn("brewery_id");
		});

		Schema::drop("vats");
	}

}
