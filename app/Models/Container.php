<?php 

namespace App\Models;

use App\Http\Interfaces\ManagementableObjectInterface;
use App\Http\Requests\AddEditContainerRequest;
use App\Services\ManagementService;

abstract class Container extends Model implements ManagementableObjectInterface {

	protected $table;
	protected $fillable = ["name", "capacity", "brewery_id"];
	public $timestamps = false;

	public function brewery() {
		return $this->belongsTo(Brewery::class);
	}

	public function getRequest() {
		return new AddEditContainerRequest();
	}

	public function getService() {
		return new ManagementService();
	}

}
