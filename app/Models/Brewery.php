<?php 

namespace App\Models;

use App\Http\Interfaces\ManagementableObjectInterface;
use App\Http\Requests\AddEditBreweryRequest;
use App\Services\BreweryManagementService;
use Carbon\Carbon;

class Brewery extends Model implements ManagementableObjectInterface {

	protected $table = "breweries";
	protected $fillable = ["name", "user_id"];

	public function batches() {
		return $this->hasMany(Batch::class);
	}

	public function ongoing_batches() {
		return $this->batches()->where("brewing_date", "<=", Carbon::now());
	}

	public function planned_batches() {
		return $this->batches()->where("brewing_date", ">", Carbon::now());
	}

	public function vats() {
		return $this->hasMany(Vat::class);
	}

	public function fermenters() {
		return $this->hasMany(Fermenter::class);
	}

	public function getVatsCountAttribute() {
		return $this->vats->count();
	}

	public function getFermentersCountAttribute() {
		return $this->fermenters->count();
	}

	public function getBrewedBatchesCountAttribute() {
		return $this->batches->count();
	}

	public function getOngoingBatchesCountAttribute() {
		return $this->ongoing_batches->count();
	}

	public function getPlannedBatchesCountAttribute() {
		return $this->planned_batches->count();
	}

	public function getRequest() {
		return new AddEditBreweryRequest();
	}

	public function getService() {
		return new BreweryManagementService();
	}

}
