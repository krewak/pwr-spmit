<?php 

namespace App\Models;

class Fermenter extends Container {

	protected $table = "fermenters";

	public function batchesOnPrimary() {
		return $this->hasMany(Batch::class, "primary_fermenter_id");
	}

	public function batchesOnSecondary() {
		return $this->hasMany(Batch::class, "secondary_fermenter_id");
	}

}
