<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

	protected $fillable = ["name", "email", "password", "current_brewery_id"];
	protected $hidden = ["password", "remember_token"];

	public function breweries() {
		return $this->hasMany(Brewery::class);
	}

	public function getCurrentBrewery() {
		return $this->breweries->where("id", $this->current_brewery_id)->first();
	}

	public function isAdmin() {
		return $this->is_admin;
	}

}
