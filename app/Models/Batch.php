<?php 

namespace App\Models;

use App\Http\Interfaces\ManagementableObjectInterface;
use App\Http\Requests\AddEditBatchRequest;
use App\Services\ManagementService;

class Batch extends Model implements ManagementableObjectInterface {

	protected $table = "batches";
	protected $fillable = ["name", "brewery_id", "style_id", "status", "brewing_date", "primary_days", "secondary_days", "volume"];
	public $timestamps = false;

	const PLANNED = 1;

	public function brewery() {
		return $this->belongsTo(Brewery::class);
	}

	public function style() {
		return $this->belongsTo(BeerStyle::class);
	}

	public function primaryFermenter() {
		return $this->belongsTo(Fermenter::class, "primary_fermenter_id");
	}

	public function secondaryFermenter() {
		return $this->belongsTo(Fermenter::class, "secondary_fermenter_id");
	}

	public function getRequest() {
		return new AddEditBatchRequest();
	}

	public function getService() {
		return new ManagementService();
	}

}
