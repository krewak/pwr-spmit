<?php 

namespace App\Models;

class BeerStyle extends Model {

	protected $table;
	protected $fillable = ["symbol", "name", "brewery_id", "abv_min", "abv_max", "og_min", "og_max", "fg_min", "fg_max", "ibu_min", "ibu_max", "srm_min", "srm_max"];
	public $timestamps = false;

	public function convertToPlato($value) {
		if(is_null($value)) {
			return null;
		}

		// based on scientific/chemistry mumbo-jumbo
		$result = -616.868;
		$result += 1111.14 * $value;
		$result -= 630.272 * pow($value, 2);
		$result += 135.997 * pow($value, 3);

		return $result > 0 ? number_format($result, 1) : 0;
	}

	public function getOgMinPlatoAttribute() {
		return $this->convertToPlato($this->og_min);
	}

	public function getOgMaxPlatoAttribute() {
		return $this->convertToPlato($this->og_max);
	}

	public function getFgMinPlatoAttribute() {
		return $this->convertToPlato($this->fg_min);
	}

	public function getFgMaxPlatoAttribute() {
		return $this->convertToPlato($this->fg_max);
	}

}
