<?php 

namespace App\Services;

use Auth;

class BreweryManagementService extends ManagementService {

	public function run() {
		$brewery = $this->object;
		$brewery->fill($this->data);
		$run = $brewery->save();

		if($run && is_null(Auth::user()->getCurrentBrewery())) {
			Auth::user()->current_brewery_id = $brewery->id;
			Auth::user()->save();
		}

		return $run;
	}

	public function delete() {
		$object = $this->object;

		if(Auth::user()->current_brewery_id == $object->id) {
			Auth::user()->current_brewery_id = null;
			Auth::user()->save();
		}

		$object->delete();
		return $this;
	}

}
