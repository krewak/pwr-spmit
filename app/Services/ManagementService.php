<?php 

namespace App\Services;

class ManagementService {

	protected $data;
	protected $object;

	public function setData($request) {
		// filter empty values
		$this->data = array_filter($request->except("_token"), "strlen");
		return $this;
	}

	public function setObject($object) {
		$this->object = $object;
		return $this;
	}

	public function run() {
		$object = $this->object;
		$object->fill($this->data);
		return $object->save();
	}

	public function delete() {
		$object = $this->object;
		$object->delete();
		return $this;
	}

}
