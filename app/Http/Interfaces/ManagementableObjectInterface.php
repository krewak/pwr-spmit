<?php

namespace App\Http\Interfaces;

interface ManagementableObjectInterface
{

	public function getRequest();
	
	public function getService();

}