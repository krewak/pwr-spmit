<?php

namespace App\Http\Interfaces;

interface ManagementSystemInterface
{

	public function getColumns();
	
	public function getEditableFields();
	
	public function getObjectSet();

	public function getNewObject();
	
	public function getSearchQuery($search_phrase);

}