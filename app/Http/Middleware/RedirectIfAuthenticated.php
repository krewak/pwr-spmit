<?php

namespace App\Http\Middleware;

use Closure, Redirect, Session;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {
    
    public function handle($request, Closure $next, $guard = null) {

        if(Auth::guard($guard)->check()) {
            Session::flash("error", "Niestety nie masz dostępu do tej części witryny.");
            return Redirect::route("home");
        }

        return $next($request);
    }
}
