<?php

namespace App\Http\Middleware;

use Closure, Redirect, Session;
use Illuminate\Support\Facades\Auth;

class AuthenticateWithAllPermissions
{
	public function handle($request, Closure $next, $guard = null)
	{
		if(!Auth::user()->isAdmin()) {
			if($request->ajax() || $request->wantsJson()) {
				return response("Unauthorized.", 401);
			} else {
				Session::flash("error", "Nie masz dostępu do tej części serwisu.");
				return Redirect::route("home");
			}
		}

		return $next($request);
	}
}
