<?php 

namespace App\Http\Traits;

use Exception, Redirect, Session, Validator;
use App\Http\Interfaces\ManagementableObjectInterface;
use Illuminate\Http\Request;

trait ManagementMethods {

	public function getForm($object) {
		if(!($this->getNewObject() instanceof ManagementableObjectInterface)) {
			throw new Exception("Obiekt nie jest dostosowany do zarządzania. Zaimplementuj ManagementableObjectInterface", 1);
		}

		return view("cms.addedit", [
			"object" => $object,
			"fields" => $this->getEditableFields(),
			"item_name" => $this->item_name,

			"route" => $this->route,
			"current_page" => $this->route,
		]);
	}


	public function getAddForm() {
		return $this->getForm($this->getNewObject());
	}

	public function getEditForm($id) {
		$object = $this->getObjectSet()->find($id);

		if(!$object) {
			Session::flash("error", "Podano błędny identyfikator obiektu."); 
			return Redirect::route("dashboard.". $this->route .".list");
		}

		return $this->getForm($object);
	}

	public function post(Request $request, $object) {
		if(!($this->getNewObject() instanceof ManagementableObjectInterface)) {
			throw new Exception("Obiekt nie jest dostosowany do zarządzania. Zaimplementuj ManagementableObjectInterface", 1);
		}

		$request_validator = $object->getRequest();
		$validator = Validator::make($request->all(), $request_validator->rules(), $request_validator->messages());

		if($validator->fails()) {
			$build_error_message = "";
			foreach ($validator->messages()->all() as $message) {
				$build_error_message .= "<li>". $message ."</li>";
			}

			$request->flash();
			Session::flash("error", $build_error_message); 

			throw new Exception("Wystąpiły błędy walidacji.", 1);
		}

		if(!$object->getService()->setObject($object)->setData($request)->run()) {
			throw new Exception("Nie udało się zapisać obiektu.", 2);
		}
	}

	public function postAdd(Request $request) {
		try {
			$view = $this->post($request, $this->getNewObject());
		} catch(Exception $e) {
			return Redirect::route("dashboard.". $this->route .".add");
		}

		if($view) {
			return $view;
		}

		Session::flash("success", "Obiekt dodano poprawnie do bazy danych."); 
		return Redirect::route("dashboard.". $this->route .".list");
	}

	public function postEdit(Request $request, $id) {
		$object = $this->getObjectSet()->find($id);

		if(!$object) {
			Session::flash("error", "Podano błędny identyfikator obiektu."); 
			return Redirect::route("dashboard.". $this->route .".list");
		}
		
		try {
			$this->post($request, $object);
		} catch(Exception $e) {
			return Redirect::route("dashboard.". $this->route .".edit", $object->id);
		}

		Session::flash("success", "Obiekt poprawnie edytowano."); 
		return Redirect::route("dashboard.". $this->route .".list");
	}

	public function getDeleteConfirm($id) {
		$object = $this->getObjectSet()->find($id);

		if(!$object) {
			Session::flash("error", "Podano błędny identyfikator obiektu."); 
			return Redirect::route("dashboard.". $this->route .".list");
		}

		return view("cms.delete", [
			"object" => $object,
			"item_name" => $this->item_name,

			"route" => $this->route,
			"current_page" => $this->route,
		]);
	}

	public function postDelete($id) {
		$object = $this->getObjectSet()->find($id);

		if(!$object) {
			Session::flash("error", "Podano błędny identyfikator obiektu."); 
			return Redirect::route("dashboard.". $this->route .".list");
		}

		$object->getService()->setObject($object)->delete();
		Session::flash("success", "Obiekt został usunięty poprawnie."); 
		return Redirect::route("dashboard.". $this->route .".list");
	}

}