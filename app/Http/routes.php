<?php

Route::group(["middleware" => ["web"]], function() {

	Route::get("/", ["as" => "home", "uses" => "HomeController@getHome"]);

	Route::group(["middleware" => ["guest"]], function() {

		Route::post("/login", ["as" => "login", "uses" => "AuthController@postLogin"]);

	});

	Route::group(["middleware" => ["auth"]], function() {

		Route::get("/dashboard", ["as" => "dashboard", "uses" => "DashboardController@getDashboard"]);

		Route::get("/dashboard/current/change", ["as" => "dashboard.current", "uses" => "DashboardController@getCurrentBrewery"]);
		Route::post("/dashboard/current/set", ["as" => "dashboard.current.set", "uses" => "DashboardController@postCurrentBrewery"]);
		
		Route::get("/dashboard/plans", ["as" => "dashboard.plans", "uses" => "PlannedBatchManagementSystemController@getPlans"]);
		Route::post("/checkfile", ["as" => "checkfile", "uses" => "PlannedBatchManagementSystemController@checkIfOutFileHasBeenGenerated"]);

		$managementable_models = ["vat", "fermenter", "brewery", "ongoing.batch", "planned.batch"];
		foreach($managementable_models as $model) {
			getManagementRoutes($model);
		}

		Route::get("/dashboard/batch/add", ["as" => "dashboard.batch.add", "uses" => "PlannedBatchManagementSystemController@getAddForm"]);
		Route::post("/dashboard/batch/add", ["as" => "dashboard.batch.add.post", "uses" => "PlannedBatchManagementSystemController@postAdd"]);
		
		Route::post("/logout", ["as" => "logout", "uses" => "AuthController@postLogout"]);

		Route::group(["middleware" => ["admin"]], function() {
			Route::get("/dashboard/admin", ["as" => "dashboard.admin", "uses" => "DashboardController@getAdminDashboard"]);

			$managementable_models = ["user", "style"];
			foreach($managementable_models as $model) {
				getManagementRoutes($model);
			}
		});

	});

});

function getManagementRoutes($model) {
	$controller_name = str_replace(".", "", ucfirst($model));

	Route::get("/dashboard/". $model, [
		"as" => "dashboard.". $model .".list",
		"uses" => $controller_name ."ManagementSystemController@getList"
	]);
	Route::get("/dashboard/". $model ."/{id}", [
		"as" => "dashboard.". $model .".view",
		"uses" => $controller_name ."ManagementSystemController@getView"
	])->where("id", "[0-9]+");
	Route::get("/dashboard/". $model ."/add", [
		"as" => "dashboard.". $model .".add",
		"uses" => $controller_name ."ManagementSystemController@getAddForm"
	]);
	Route::post("/dashboard/". $model ."/add", [
		"as" => "dashboard.". $model .".add.post",
		"uses" => $controller_name ."ManagementSystemController@postAdd"
	]);
	Route::get("/dashboard/". $model ."/edit/{id}", [
		"as" => "dashboard.". $model .".edit",
		"uses" => $controller_name ."ManagementSystemController@getEditForm"
	])->where("id", "[0-9]+");
	Route::post("/dashboard/". $model ."/edit/{id}", [
		"as" => "dashboard.". $model .".edit.post",
		"uses" => $controller_name ."ManagementSystemController@postEdit"
	])->where("id", "[0-9]+");
	Route::get("/dashboard/". $model ."/delete/{id}", [
		"as" => "dashboard.". $model .".delete",
		"uses" => $controller_name ."ManagementSystemController@getDeleteConfirm"
	])->where("id", "[0-9]+");
	Route::post("/dashboard/". $model ."/delete/{id}", [
		"as" => "dashboard.". $model .".delete.post",
		"uses" => $controller_name ."ManagementSystemController@postDelete"
	])->where("id", "[0-9]+");
	Route::get("/dashboard/". $model ."/find", [
		"as" => "dashboard.". $model .".find",
		"uses" => $controller_name ."ManagementSystemController@getSearchJSON"
	]);
}