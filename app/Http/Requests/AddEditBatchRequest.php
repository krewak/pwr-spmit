<?php 

namespace App\Http\Requests;

class AddEditBatchRequest extends Request {

	public function rules() {
		return [
			"id" => "exists:batches",
			"name" => "required|min:1",
			"style_id" => "required|exists:beer_styles,id",
			"volume" => "required|integer|min:1",
			"primary_days" => "required|integer|min:1",
			"secondary_days" => "required|integer|min:1",
		];
	}

	public function messages() {
		return [			
			"id.exists" => "Podano błędny identyfikator obiektu.",
			"name.required" => "Nazwa jest wymagana.",

			"name.min" => "Nazwa musi mieć przynajmniej jeden znak.",
			"name.required" => "Nazwa jest wymagana.",

			"style_id.exists" => "Styl musi istnieć.",
			"style_id.required" => "Styl jest wymagana.",

			"volume.min" => "Objętość musi mieć przynajmniej jeden znak.",
			"volume.integer" => "Objętość musi być liczbą.",
			"volume.required" => "Objętość jest wymagana.",

			"primary_days.min" => "Liczba dni burzliwej fermentacji musi mieć przynajmniej jeden znak.",
			"primary_days.integer" => "Liczba dni burzliwej fermentacji musi być liczbą.",
			"primary_days.required" => "Liczba dni burzliwej fermentacji jest wymagana.",

			"secondary_days.min" => "Liczba dni cichej fermentacji musi mieć przynajmniej jeden znak.",
			"secondary_days.integer" => "Liczba dni cichej fermentacji musi być liczbą.",
			"secondary_days.required" => "Liczba dni cichej fermentacji jest wymagana.",
		];
	}

}
