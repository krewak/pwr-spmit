<?php 

namespace App\Http\Requests;

class LoginRequest extends Request {

	public function rules() {
		return [
			"name" => "required",
			"password" => "required"
		];
	}

}
