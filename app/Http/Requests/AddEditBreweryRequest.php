<?php 

namespace App\Http\Requests;

class AddEditBreweryRequest extends Request {

	public function rules() {
		return [
			"id" => "exists:breweries",
			"name" => "required|min:1",
		];
	}

	public function messages() {
		return [			
			"id.exists" => "Podano błędny identyfikator obiektu.",
			"name.required" => "Nazwa jest wymagana.",
			"name.min" => "Nazwa musi mieć przynajmniej jeden znak.",
		];
	}

}
