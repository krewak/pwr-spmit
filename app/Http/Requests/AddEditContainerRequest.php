<?php 

namespace App\Http\Requests;

class AddEditContainerRequest extends Request {

	public function rules() {
		return [
			"id" => "exists:vats",
			"name" => "required|min:1",
			"capacity" => "required|numeric",
		];
	}

	public function messages() {
		return [			
			"id.exists" => "Podano błędny identyfikator obiektu.",
			"name.required" => "Nazwa jest wymagana.",
			"name.min" => "Nazwa musi mieć przynajmniej jeden znak.",
			"capacity.required" => "Pojemność jest wymagana.",
			"capacity.numeric" => "Pojemność musi być liczbą.",
		];
	}

}
