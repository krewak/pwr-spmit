<?php 

namespace App\Http\Controllers;

use Auth;
use App\Models\User;

class UserManagementSystemController extends AbstractManagementSystemController {

	protected $title = "Lista zarejestrowanych użytkowników";
	protected $item_name = "użytkownik";
	protected $route = "user";

	protected $order = "name-asc";
	protected $allowed_orders = [
		"id" => "id",
		"name" => "name",
		"is_admin" => "is_admin",
	];
	protected $allowed_actions = [
		"edit" => false,
		"delete" => true,
		"preview" => false,
	];

	public function getColumns() {
		return [
			[
				"name" => "id",
				"title" => "id",
				"sortable" => true,
				"value" => function($user) {
					return $user->id;
				}
			],
			[
				"name" => "name",
				"title" => "nazwa",
				"sortable" => true,
				"value" => function($user) {
					return $user->name;
				}
			],
			[
				"name" => "created_at",
				"title" => "data rejestracji",
				"sortable" => true,
				"value" => function($user) {
					return $user->created_at;
				}
			],
			[
				"name" => "is_admin",
				"title" => "administrator?",
				"boolean" => true,
				"sortable" => true,
				"value" => function($user) {
					return $user->isAdmin();
				}
			],
		];
	}

	public function getEditableFields() {
		return [
			[
				"name" => "name",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz nazwę użytkownika",
				"value" => function($user) {
					return isset($user->id) ? $user->name : null;
				}
			],
		];
	}

	public function getObjectSet() {
		return User::orderBy("is_admin", "desc");
	}

	public function getNewObject() {
		return new User;
	}
	
	public function getSearchQuery($search_phrase) {
		return $this->getObjectSet()->select("id", "name")
			->where("name", "LIKE", "%". $search_phrase ."%")
			->orderBy("name", "asc")
			->get();
	}

}
