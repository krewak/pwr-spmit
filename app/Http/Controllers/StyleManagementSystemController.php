<?php 

namespace App\Http\Controllers;

use App\Models\BeerStyle;

class StyleManagementSystemController extends AbstractManagementSystemController {

	protected $title = "Lista stylów piwa (wg BJCP 2015)";
	protected $item_name = "styl";
	protected $route = "style";

	protected $order = "symbol-asc";
	protected $allowed_orders = [
		"symbol" => "symbol",
		"name" => "name",
		"abv_min" => "abv_min",
		"abv_max" => "abv_max",
		"og_min" => "og_min",
		"og_max" => "og_max",
		"fg_min" => "fg_min",
		"fg_max" => "fg_max",
		"ibu_min" => "ibu_min",
		"ibu_max" => "ibu_max",
		"srm_min" => "srm_min",
		"srm_max" => "srm_max",
	];

	public function getColumns() {
		return [
			[
				"name" => "symbol",
				"title" => "symbol",
				"sortable" => true,
				"value" => function($style) {
					if($style->symbol[0] == "0") {
						return substr($style->symbol, 1);
					}

					return $style->symbol;
				}
			],
			[
				"name" => "name",
				"title" => "nazwa",
				"sortable" => true,
				"value" => function($style) {
					return $style->name;
				}
			],
			[
				"name" => "abv",
				"title" => "abv",
				"sortable" => true,
				"minmax" => true,
				"value" => function($style) {
					return [
						"min" => !is_null($style->abv_min) ? number_format($style->abv_min, 1) : null,
						"max" => !is_null($style->abv_max) ? number_format($style->abv_max, 1) : null,
					];
				}
			],
			[
				"name" => "og",
				"title" => "og",
				"sortable" => true,
				"minmax" => true,
				"value" => function($style) {
					return [
						"min" => $style->og_min_plato,
						"max" => $style->og_max_plato,
					];
				}
			],
			[
				"name" => "fg",
				"title" => "fg",
				"sortable" => true,
				"minmax" => true,
				"value" => function($style) {
					return [
						"min" => $style->fg_min_plato,
						"max" => $style->fg_max_plato,
					];
				}
			],
			[
				"name" => "ibu",
				"title" => "ibu",
				"sortable" => true,
				"minmax" => true,
				"value" => function($style) {
					return [
						"min" => $style->ibu_min,
						"max" => $style->ibu_max,
					];
				}
			],
			[
				"name" => "srm",
				"title" => "srm",
				"sortable" => true,
				"minmax" => true,
				"value" => function($style) {
					return [
						"min" => $style->srm_min,
						"max" => $style->srm_max,
					];
				}
			],
		];
	}

	public function getEditableFields() {
		return [
			[
				"name" => "symbol",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz symbol",
				"value" => function($user) {
					return isset($user->id) ? $user->symbol : null;
				}
			],
			[
				"name" => "name",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz nazwę",
				"value" => function($user) {
					return isset($user->id) ? $user->name : null;
				}
			],
		];
	}

	public function getObjectSet() {
		return new BeerStyle;
	}

	public function getNewObject() {
		return new BeerStyle;
	}
	
	public function getSearchQuery($search_phrase) {
		return $this->getObjectSet()->select("id", "name")
			->where("name", "LIKE", "%". $search_phrase ."%")
			->orderBy("name", "asc")
			->get();
	}

}
