<?php 

namespace App\Http\Controllers;

use Auth, Session;
use App\Http\Controllers\AuthController, App\Http\Controllers\DashboardController;

class HomeController extends Controller {

	public function getHome() {
		if(Auth::user()) {
			return DashboardController::getDashboard();
		}

		return AuthController::getLogin();
	}

}
