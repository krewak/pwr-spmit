<?php 

namespace App\Http\Controllers;

use Input, Response;
use App\Http\Interfaces\ManagementSystemInterface;
use App\Http\Traits\ManagementMethods;

abstract class AbstractManagementSystemController extends Controller implements ManagementSystemInterface {

	use ManagementMethods;

	protected $title;
	protected $description;
	protected $item_name;
	protected $route;

	protected $order = "id-asc";
	protected $filter = null;

	protected $allow_ordinal_column = false;
	protected $allow_new_items = false;
	protected $allowed_orders;
	protected $allowed_actions = [
		"edit" => true,
		"delete" => true,
		"preview" => true,
	];

	public function getData($order) {
		$order = $this->prepareOrder($order);
		return $this->getObjectSet()->orderBy($order[0], $order[1]);
	}

	public function validateOrder($order) {
		$order = explode("-", $order);
		if(sizeof($order) != 2) {
			return false;
		}

		if(!in_array($order[0], array_keys($this->allowed_orders))) {
			return false;
		}

		if(!in_array($order[1], ["asc", "desc"])) {
			return false;
		}

		return true;
	}

	public function prepareOrder($order) {
		if($this->validateOrder($order)) {
			$order = explode("-", $order);
			$order[0] = $this->allowed_orders[$order[0]];
			return $order;
		}
		return explode("-", $this->order);
	}

	public function getList() {
		$order = Input::get("order") ?: $this->order;
		$data = $this->getData($order);

		return view("cms.list", [
			"columns" => $this->getColumns(),
			"data" => $data->get(),

			"title" => $this->title,
			"description" => $this->description,
			"route" => $this->route,
			
			"order" => $this->validateOrder($order) ? $order : $this->order,
			"filter" => $this->filter,

			"allow_ordinal_column" => $this->allow_ordinal_column ? 1 : 0,
			"allow_new_items" => $this->allow_new_items,
			"allowed_actions" => $this->allowed_actions,
			"any_actions" => function() {
				foreach($this->allowed_actions as $action) {
					if($action) return true;
				}
				return false;
			},

			"current_page" => $this->route,
		]);
	}

	public function getSearchJSON() {
		$query = $this->getSearchQuery(Input::get("query"));

		return Response::json($query);
	}

}
