<?php 

namespace App\Http\Controllers;

use Auth, Redirect, Session;
use Illuminate\Http\Request;

class DashboardController extends Controller {
	
	public static function getDashboard() {
		if(is_null(Auth::user()->getCurrentBrewery())) {
			return view("dashboard.current");
		}
		return view("dashboard.dashboard");
	}

	public function getCurrentBrewery() {
		return view("dashboard.current");
	}

	public function postCurrentBrewery(Request $request) {
		$brewery = Auth::user()->breweries->where("id", (int) $request->input("brewery_id"))->first();

		if(is_null($brewery)) {
			Session::flash("error", "Nie masz praw do zarządzania wybranych browarem");
			return self::getDashboard();
		}

		Auth::user()->update(["current_brewery_id" => $brewery->id]);

		Session::flash("success", "Wybrano <strong>". $brewery->name ."</strong>.");
		return Redirect::route("dashboard");
	}

	public static function getAdminDashboard() {
		return view("dashboard.admin");
	}

}
