<?php

namespace App\Http\Controllers;

use Auth, Redirect, Session;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;

class AuthController extends Controller {

	public static function getLogin() {
		return view("login");
	}

	public function postLogin(LoginRequest $request) {
		$userdata = [
			"name" => $request->get("name"),
			"password" => $request->get("password")
		];

		if(Auth::attempt($userdata, !is_null($request->get("remember")))) {
			Session::flash("success", "Poprawnie zalogowano do systemu."); 
			return Redirect::route("dashboard");
		} else {
			Session::flash("error", "Wprowadzono błędny login lub hasło."); 
			return Redirect::route("home");
		}
	}

	public function postLogout() {
		Auth::logout();
		Session::flash("success", "Wylogowano poprawnie z systemu."); 
		return Redirect::route("home");
	}

}
