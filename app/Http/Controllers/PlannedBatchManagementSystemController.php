<?php 

namespace App\Http\Controllers;

use App\Models\Batch;
use Auth, Response, Storage, View;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class PlannedBatchManagementSystemController extends BatchManagementSystemController {

	protected $title = "Lista zaplanowanych warek";
	protected $route = "planned.batch";

	public function getObjectSet() {
		return Auth::user()->getCurrentBrewery()->planned_batches();
	}

	public function getPlans() {
		$batch = Batch::first();
		$fermenters = [];

		$one_year_forward = Carbon::today()->addYear();

		foreach(Auth::user()->getCurrentBrewery()->fermenters as $fermenter) {
			$fermenter_stash = [];

			$primary_batches = $fermenter->batchesOnPrimary()->orderBy("brewing_date")->get();
			$secondary_batches = $fermenter->batchesOnSecondary()->orderBy("brewing_date")->get();

			foreach($secondary_batches as $batch) {
				$batch->brewing_date = (new Carbon($batch->brewing_date))->addDay($batch->primary_days);
			}
			$secondary_batches = $secondary_batches->sortBy("brewing_date");

			$iteration_date = Carbon::today();
			$availability = "";

			$primary_fermentation_days_left = 0;
			$secondary_fermentation_days_left = 0;

			while(!$iteration_date->eq($one_year_forward)) {

				if(!$primary_fermentation_days_left) {
					$value = 0;
				} else {
					$value = 1;
					$primary_fermentation_days_left--;
				}

				if(!$value && !$primary_batches->isEmpty()) {
					if($iteration_date->eq(new Carbon($primary_batches->first()->brewing_date))) {
						$batch = $primary_batches->shift();
						$primary_fermentation_days_left = $batch->primary_days - 1;
						$value = 1;
					}
				}

				if($secondary_fermentation_days_left) {
					$value = 1;
					$secondary_fermentation_days_left--;
				}

				if(!$value && !$secondary_batches->isEmpty()) {
					if($iteration_date->eq(new Carbon($secondary_batches->first()->brewing_date))) {
						$batch = $secondary_batches->shift();
						$secondary_fermentation_days_left = $batch->secondary_days - 1;
						$value = 1;
					}
				}

				$availability .= $value;
				array_push($fermenter_stash, ["batch" => $batch->id, "value" =>$value]);
				$iteration_date = $iteration_date->addDay();
			}

			array_push($fermenters, ["fermenter" => $fermenter, "days" => $fermenter_stash, "today" => Carbon::today()->addDay(-1)]);
		}

		return View::make("dashboard.plans")->with("fermenters", $fermenters)->with("batches", Auth::user()->getCurrentBrewery()->batches);
	}

	public function getRawPlannedBatches($batch) {
		$filename = "in.txt";
		$batch_id = $batch->id;

		Storage::put($filename, $batch->primary_days);
		Storage::append($filename, $batch->secondary_days);
		Storage::append($filename, "");
		Storage::append($filename, $batch->volume);
		Storage::append($filename, "");

		$one_year_forward = Carbon::today()->addYear();

		foreach(Auth::user()->getCurrentBrewery()->fermenters as $fermenter) {
			Storage::append($filename, $fermenter->capacity);

			$primary_batches = $fermenter->batchesOnPrimary()->orderBy("brewing_date")->get();
			$secondary_batches = $fermenter->batchesOnSecondary()->orderBy("brewing_date")->get();

			foreach($secondary_batches as $batch) {
				$batch->brewing_date = (new Carbon($batch->brewing_date))->addDay($batch->primary_days);
			}
			$secondary_batches = $secondary_batches->sortBy("brewing_date");
			
			$iteration_date = Carbon::today();
			$availability = "";

			$primary_fermentation_days_left = 0;
			$secondary_fermentation_days_left = 0;

			while(!$iteration_date->eq($one_year_forward)) {

				if(!$primary_fermentation_days_left) {
					$value = 0;
				} else {
					$value = 1;
					$primary_fermentation_days_left--;
				}

				if(!$value && !$primary_batches->isEmpty()) {
					if($iteration_date->eq(new Carbon($primary_batches->first()->brewing_date))) {
						$batch = $primary_batches->shift();
						$primary_fermentation_days_left = $batch->primary_days - 1;
						$value = 1;
					}
				}

				if($secondary_fermentation_days_left) {
					$value = 1;
					$secondary_fermentation_days_left--;
				}

				if(!$value && !$secondary_batches->isEmpty()) {
					if($iteration_date->eq(new Carbon($secondary_batches->first()->brewing_date))) {
						$batch = $secondary_batches->shift();
						$secondary_fermentation_days_left = $batch->secondary_days - 1;
						$value = 1;
					}
				}

				$availability .= $value;
				$iteration_date = $iteration_date->addDay();
			}

			Storage::append($filename, $availability);

		}

		$this->runMatlab();

		return View::make("dashboard.wait")->with("batch_id", $batch_id);
	}

	public function post(Request $request, $object) {
		parent::post($request, $object);
		return $this->getRawPlannedBatches($object);
	}

	protected function runMatlab() {
		$file_path = public_path() ."\\exec\\optimize.m";
		$cmd = '"C:\\Program Files\\MATLAB\\R2012b\\bin\\matlab.exe" -nodisplay -nosplash -nodesktop -r "run '. $file_path .'"';
		shell_exec($cmd);
	}

	public function checkIfOutFileHasBeenGenerated(Request $request) {
		$data = ["success" => true];;

		try {
			$file = Storage::get("out.txt");
			$this->populateDatabaseWithFileContent($file, $request->get("batch_id"));
			$data["url"] = route("dashboard.plans");
			Storage::delete(["in.txt", "out.txt"]);
		} catch (Exception $e) {
			$data = ["success" => false];
		}

		return Response::json($data);
	}

	public function populateDatabaseWithFileContent($file, $batch_id) {
		$batch = Batch::find($batch_id);

		$content = explode("\n", $file);
		$c_max = array_shift($content);
		array_shift($content);

		$availabilities = [];
		while(true) {
			if(!$content[0]) {
				break;
			}
			$fermenter_volume = array_shift($content);
			array_push($availabilities, array_shift($content));
			array_shift($content);
		}

		$fermenters = Auth::user()->getCurrentBrewery()->fermenters;

		foreach($availabilities as $availability) {
			$fermenter = $fermenters->shift();
			if(str_contains($availability, "2")) {
				$batch->brewing_date = Carbon::now()->addDay(strpos($availability, "2"));
				$batch->primary_fermenter_id = $fermenter->id;
				$batch->save();
			}
			if(str_contains($availability, "3")) {
				$batch->secondary_fermenter_id = $fermenter->id;
				$batch->save();
			}
		}
	}

}
