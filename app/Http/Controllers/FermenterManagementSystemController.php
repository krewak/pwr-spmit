<?php 

namespace App\Http\Controllers;

use Auth;
use App\Models\Fermenter;

class FermenterManagementSystemController extends ContainerManagementSystemController {

	protected $title = "Lista fermentorów";
	protected $item_name = "fermenter";
	protected $route = "fermenter";


	public function getObjectSet() {
		return Auth::user()->getCurrentBrewery()->fermenters();
	}

	public function getNewObject() {
		$vat = new Fermenter;
		$vat->brewery_id = Auth::user()->getCurrentBrewery()->id;
		
		return $vat;
	}

}
