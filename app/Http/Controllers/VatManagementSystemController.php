<?php 

namespace App\Http\Controllers;

use Auth;
use App\Models\Vat;

class VatManagementSystemController extends ContainerManagementSystemController {

	protected $title = "Lista kadzi zacierno-warzelnych";
	protected $item_name = "kadź";
	protected $route = "vat";

	public function getObjectSet() {
		return Auth::user()->getCurrentBrewery()->vats();
	}

	public function getNewObject() {
		$vat = new Vat;
		$vat->brewery_id = Auth::user()->getCurrentBrewery()->id;
		
		return $vat;
	}

}
