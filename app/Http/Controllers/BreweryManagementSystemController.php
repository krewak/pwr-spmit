<?php 

namespace App\Http\Controllers;

use App\Models\Brewery;
use Auth;

class BreweryManagementSystemController extends AbstractManagementSystemController {

	protected $title = "Lista browarów";
	protected $item_name = "browar";
	protected $route = "brewery";

	protected $order = "name-asc";
	protected $allowed_orders = [
		"name" => "name",
	];

	public function getColumns() {
		return [
			[
				"name" => "name",
				"title" => "nazwa",
				"sortable" => true,
				"value" => function($brewery) {
					return $brewery->name;
				}
			],
		];
	}

	public function getEditableFields() {
		return [
			[
				"name" => "name",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz nazwę",
				"value" => function($brewery) {
					return isset($brewery->id) ? $brewery->name : null;
				}
			],
		];
	}

	public function getObjectSet() {
		return Auth::user()->breweries();
	}

	public function getNewObject() {
		$brewery = new Brewery;
		$brewery->user_id = Auth::user()->id;
		
		return $brewery;
	}
	
	public function getSearchQuery($search_phrase) {
		return $this->getObjectSet()->select("id", "name")
			->where("name", "LIKE", "%". $search_phrase ."%")
			->orderBy("name", "asc")
			->get();
	}

}
