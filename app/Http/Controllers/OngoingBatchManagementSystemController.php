<?php 

namespace App\Http\Controllers;

use Auth;

class OngoingBatchManagementSystemController extends BatchManagementSystemController {

	protected $title = "Lista aktualnie prowadzonych warek";
	protected $route = "ongoing.batch";

	public function getObjectSet() {
		return Auth::user()->getCurrentBrewery()->ongoing_batches();
	}

}
