<?php 

namespace App\Http\Controllers;

use Auth;

abstract class ContainerManagementSystemController extends AbstractManagementSystemController {

	protected $order = "name-asc";

	protected $allow_ordinal_column = true;
	protected $allow_new_items = true;
	protected $allowed_orders = [
		"id" => "id",
		"name" => "name",
		"capacity" => "capacity",
	];

	public function getColumns() {
		return [
			[
				"name" => "name",
				"title" => "nazwa",
				"sortable" => true,
				"value" => function($container) {
					return $container->name;
				}
			],
			[
				"name" => "capacity",
				"title" => "pojemność [l]",
				"sortable" => true,
				"value" => function($container) {
					return $container->capacity;
				}
			],
		];
	}

	public function getEditableFields() {
		return [
			[
				"name" => "name",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz nazwę pojemnika",
				"value" => function($container) {
					return isset($container->id) ? $container->name : null;
				}
			],
			[
				"name" => "capacity",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz pojemność pojemnika",
				"value" => function($container) {
					return isset($container->id) ? $container->capacity : null;
				}
			],
		];
	}
	
	public function getSearchQuery($search_phrase) {
		return $this->getObjectSet()->select("id", "name")
			->where("name", "LIKE", "%". $search_phrase ."%")
			->orderBy("name", "asc")
			->get();
	}

}
