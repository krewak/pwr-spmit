<?php 

namespace App\Http\Controllers;

use App\Models\Batch;
use App\Models\BeerStyle;
use Auth;

abstract class BatchManagementSystemController extends AbstractManagementSystemController {

	protected $title = "Lista warek";
	protected $item_name = "warkę";
	protected $route = "batch";

	protected $order = "brewing_date-desc";
	protected $allow_new_items = false;
	protected $allowed_orders = [
		"brewing_date" => "brewing_date",
	];
	protected $allowed_actions = [
		"edit" => false,
		"delete" => true,
		"preview" => false,
	];

	public function getColumns() {
		return [
			[
				"name" => "name",
				"title" => "nazwa",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->name;
				}
			],
			[
				"name" => "volume",
				"title" => "objętość [l]",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->volume;
				}
			],
			[
				"name" => "primary",
				"title" => "data",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->brewing_date;
				}
			],
			[
				"name" => "primary",
				"title" => "fermentacja burzliwa",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->primaryFermenter->name;
				}
			],
			[
				"name" => "primary",
				"title" => "czas [d]",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->primary_days;
				}
			],
			[
				"name" => "primary",
				"title" => "fermentacja cicha",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->secondaryFermenter->name;
				}
			],
			[
				"name" => "primary",
				"title" => "czas [d]",
				"sortable" => true,
				"value" => function($batch) {
					return $batch->secondary_days;
				}
			],
		];
	}

	public function getEditableFields() {
		return [
			[
				"name" => "name",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz nazwę",
				"value" => function($batch) {
					return isset($batch->id) ? $batch->name : null;
				}
			],
			[
				"name" => "style_id",
				"type" => "select",
				"required" => true,
				"label" => "Wybierz styl",
				"options" => BeerStyle::all(),
				"value" => function($batch) {
					return isset($batch->id) ? $batch->style_id : null;
				}
			],
			[
				"name" => "volume",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz objętość (wyrażoną w litrach)",
				"value" => function($batch) {
					return isset($batch->id) ? $batch->volume : null;
				}
			],
			[
				"name" => "primary_days",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz liczbę dni burzliwej fermentacji",
				"value" => function($batch) {
					return isset($batch->id) ? $batch->primary_days : null;
				}
			],
			[
				"name" => "secondary_days",
				"type" => "text",
				"required" => true,
				"label" => "Wpisz liczbę dni cichej fermentacji",
				"value" => function($batch) {
					return isset($batch->id) ? $batch->secondary_days : null;
				}
			],
		];
	}

	public function getNewObject() {
		$batch = new Batch;
		$batch->brewery_id = Auth::user()->getCurrentBrewery()->id;
		$batch->style_id = 1;
		return $batch;
	}
	
	public function getSearchQuery($search_phrase) {
		return $this->getObjectSet()->select("id", "name")
			->where("name", "LIKE", "%". $search_phrase ."%")
			->orderBy("name", "asc")
			->get();
	}

}
