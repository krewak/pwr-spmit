@if(Session::has("success"))
	<div class="alert alert-success alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>
		<strong>Super!</strong> {!! Session::get("success") !!}
	</div>
@endif

@if(Session::has("message"))
	<div class="alert alert-info alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>
		<strong>Uwaga!</strong> {!! Session::get("message") !!}
	</div>
@endif

@if(Session::has("warning"))
	<div class="alert alert-warning alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>
		<strong>Uwaga!</strong> {!! Session::get("warning") !!}
	</div>
@endif

@if(Session::has("error"))
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>
		<strong>Błąd!</strong> {!! Session::get("error") !!}
	</div>
@endif

@if($errors->count())
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>
		@foreach($errors->all() as $error)
		{!! $error !!}
		@endforeach
	</div>
@endif
