@extends("root")

@section("styles")
	{!! Html::style("css/login.css") !!}
@stop

@section("container")
	<h2 class="app-title">Sterowanie produkcją, magazynowaniem i transportem:</h2>
	<h1 class="app-title">System optymalizujący produkcję w browarze</h1>

	<div class="login-panel col-md-6 col-md-offset-3">
		@include("alerts")
		<h3>Dostęp wyłącznie dla zalogowanych użytkowników:</h3>

		<form method="post" action="{{ route('login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="input-group">
				<span class="input-group-addon" id="login-addon"><i class="fa fa-user fa-2x"></i></span>
				<input name="name" type="text" class="form-control" placeholder="Podaj login" aria-describedby="login-addon" autofocus>
			</div>
			<div class="input-group">
				<span class="input-group-addon" id="password-addon"><i class="fa fa-lock fa-2x"></i></span>
				<input name="password" type="password" class="form-control" placeholder="Podaj hasło" aria-describedby="password-addon">
			</div>
			<div class="input-group">
				<span class="input-group-addon"></span>
				<div class="row">
					<div class="col-xs-5">
						<a href="#" class="form-control">Zarejestruj się</a>
					</div>
					<div class="col-xs-7">
						<input type="submit" class="form-control" value="Zaloguj">
					</div>
				</div>
			</div>
		</form>
	</div>
@stop