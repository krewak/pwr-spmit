@extends("dashboard.root")

@section("dashboard.container")

	<div class="row">
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="text-right">
						<div>kadzi zacierno-warzelnych</div>
						<div class="huge">{{ $current_brewery->vats_count }}</div>
					</div>
				</div>
				<a href="{{ route('dashboard.vat.list') }}">
					<div class="panel-footer">
						<span class="pull-left">zarządzaj</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="text-right">
						<div>pojemników fermentacyjnych</div>
						<div class="huge">{{ $current_brewery->fermenters_count }}</div>
					</div>
				</div>
				<a href="{{ route('dashboard.fermenter.list') }}">
					<div class="panel-footer">
						<span class="pull-left">zarządzaj</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="text-right">
						<div>warek aktualnie w produkcji</div>
						<div class="huge">{{ $current_brewery->ongoing_batches_count }}</div>
					</div>
				</div>
				<a href="{{ route('dashboard.ongoing.batch.list') }}">
					<div class="panel-footer">
						<span class="pull-left">zarządzaj</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="text-right">
						<div>warek zaplanowano</div>
						<div class="huge">{{ $current_brewery->planned_batches_count }}</div>
					</div>
				</div>
				<a href="{{ route('dashboard.planned.batch.list') }}">
					<div class="panel-footer">
						<span class="pull-left">zarządzaj</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 col-lg-offset-6 col-md-12">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="text-right">
						<div class="huge">
							<a href="{{ route('dashboard.batch.add') }}">
								<i class="fa fa-plus" style="color: White;"></i>
							</a>
						</div>
					</div>
				</div>
				<a href="{{ route('dashboard.batch.add') }}">
					<div class="panel-footer">
						<span class="pull-left">dodaj nową warkę do planu produkcji</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>
				
	<hr>

@endsection