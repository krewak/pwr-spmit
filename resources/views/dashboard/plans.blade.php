@extends("dashboard.root")

@section("dashboard.container")

	<h1>Planowanie fermentacji</h1>
	<hr>

	<div class="row well">
		Zaplanowane warki: 
		@foreach($batches as $batch)
		<div class="legend planned-day occupied" data-batch-id="{{ $batch->id }}">
				{{ $batch->id }}
		</div>
		@endforeach
	</div>

	@foreach($fermenters as $fermenter)
		<div class="fermenter well">
			<h3>{{ $fermenter["fermenter"]->name }}</h3>
			@foreach($fermenter["days"] as $day)
				<div class="planned-day {{ $day["value"] ? "occupied" : "free" }}" title="{{ $fermenter["today"]->addDay()->format("d.m.Y") }}" data-batch-id="{{ $day["value"] ? $day["batch"] : "" }}" data-toggle="tooltip" data-placement="top">
					{{ $day["value"] ? $day["batch"] : "&nbsp;" }}
				</div>
			@endforeach
		</div>
	@endforeach

@endsection