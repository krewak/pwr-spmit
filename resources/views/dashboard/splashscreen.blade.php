<div class="row">
	<h2>Wybierz browar</h2>

	@foreach($user->breweries as $brewery)
		<form action="{{ route('dashboard.current.set') }}" method="post" class="dashboard-tile change-current-brewery col-md-4 col-sm-6 col-xs-12">
			{{ $brewery->name }}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="brewery_id" value="{{ $brewery->id }}">
			<input type="submit" class="hidden">
		</form>
	@endforeach

	<div class="dashboard-tile add-new col-md-4 col-sm-6 col-xs-12" data-url="{{ route('dashboard.brewery.add') }}">
		<i class="fa fa-plus"></i> dodaj nowy
	</div>
</div>