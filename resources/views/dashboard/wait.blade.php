@extends("dashboard.root")

@section("dashboard.container")

	<h1>Planowanie fermentacji</h1>
	<hr>

	<div id="view-container" class="text-center">
		<input type="hidden" id="_token" value="{{ csrf_token() }}">
		<i id="preloader" class="fa fa-refresh fa-spin fa-4x" data-checkfile={{ route("checkfile") }} data-batch-id={{ $batch_id }}></i>
		<hr>
		<div class="alert alert-info">Trwa obliczanie...</div>
	</div>


@endsection