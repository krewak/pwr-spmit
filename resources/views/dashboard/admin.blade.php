@extends("dashboard.root")

@section("dashboard.container")

	<h1>Administracja</h1>
	<hr>

	<div class="row">
		<div class="dashboard-tile add-new col-md-4 col-sm-6 col-xs-12" data-url="{{ route('dashboard.user.list') }}">
			użytkownicy
		</div>
		<div class="dashboard-tile add-new col-md-4 col-sm-6 col-xs-12" data-url="{{ route('dashboard.style.list') }}">
			style piwne
		</div>
	</div>
				
	<hr>

@endsection