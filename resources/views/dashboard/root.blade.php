@extends("root")

@section("styles")
	{!! Html::style("css/dashboard.css") !!}
@stop

@section("navbar")
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Przełącz nawigację</span>
					<i class="fa fa-bars"></i>
				</button>
				<a class="navbar-brand" href="{{ route('dashboard') }}">SPMiT</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					@if($current_brewery)
						<li><a href="{{ route("dashboard.fermenter.list") }}">browar</a></li>
						<li><a href="{{ route("dashboard.plans") }}">plan produkcji</a></li>
						<li><a href="{{ route('dashboard.batch.add') }}"><i class="fa fa-plus"></i></a></li>
						@if($user->isAdmin())
						<li><a href="{{ route("dashboard.admin") }}">administracja</a></li>
						@endif
					@endif
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="active">
						<a href="{{ route('dashboard.current') }}">
							@if($current_brewery) {{ $current_brewery->name }} <i class="fa fa-refresh"></i> @else [wybierz browar] @endif
						</a>
					</li>
					<li><a href="#"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a></li>
					<li><a id="logout"><i class="fa fa-sign-out"></i></a></li>
					<form action="{{ route('logout') }}" method="post" class="hidden" id="logout-form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="submit">
					</form>
				</ul>
			</div>
		</div>
	</nav>
@stop

@section("container")
	@include("alerts")
	@if($current_brewery || Request::route()->getName() == "dashboard.brewery.add")
		@yield("dashboard.container")
	@else
		@include("dashboard.splashscreen")
	@endif
@stop

@section("scripts")
	{!! Html::script("js/dashboard.js") !!}
@stop