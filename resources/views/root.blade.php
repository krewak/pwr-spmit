<!DOCTYPE html>
<html lang="pl">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="AWD">
	<meta name="author" content="Krzysztof Rewak">

	<title>Sterowanie produkcją, magazynowaniem i transportem</title>
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	{!! Html::style("css/vendor/bootstrap.min.css") !!}
	{!! Html::style("css/vendor/font-awesome.min.css") !!}
	{!! Html::style("css/style.css") !!}
	@yield("styles")

</head>
<body>

	<div id="wrapper">
		@yield("navbar")

		<div class="content">
			<div class="container">
				@yield("container")
			</div>
		</div>

		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>2016 # <a href="http://rewak.eu/" target="_blank">Krzysztof Rewak</a> i <a href="http://bitbucket.org/mibalogh" target="_blank">Mihály Balogh</a></p>
					</div>
					<div class="col-md-6 text-right">
						<a href="https://trello.com/b/q5SP0ZxQ/sterowanie-produkcja-magazynowaniem-i-transportem-projekt" target="_blank"><i class="fa fa-trello fa-2x footer-icon"></i></a>
						<a href="https://bitbucket.org/krewak/pwr-spmit/" target="_blank"><i class="fa fa-bitbucket fa-2x footer-icon"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	{!! Html::script("js/vendor/jquery.min.js") !!}
	{!! Html::script("js/vendor/bootstrap.min.js") !!}
	{!! Html::script("js/script.js") !!}
	@yield("scripts")

</body>
</html>