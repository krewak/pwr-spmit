@extends("dashboard.root")

@section("dashboard.container")

	<h1>{{ $title }}</h1>

	@if($description)
	<blockquote class="description">{!! $description !!}</blockquote>
	@endif

	<div class="dashboard-table-options">
		@if($allow_new_items)
			<div class="btn-group open pull-right">
				<a class="btn btn-success" href="{{ route('dashboard.'. $route .'.add') }}"><i class="fa fa-plus"></i></a>
				<a class="btn btn-success" href="{{ route('dashboard.'. $route .'.add') }}">dodaj nowy obiekt</a>
			</div>
		@endif
	</div>

	<table class="table table-striped table-hover table-condensed">
		<thead>
			@if($allow_ordinal_column)
			<th>#</th>
			@endif
			@foreach($columns as $column)
			<th>
				@if(isset($column["minmax"]) && $column["minmax"])
					@if($column["sortable"]) 
						@if($order != $column["name"]."_min-asc")
						<a href="?{{ http_build_query(Input::except('order')) }}&order={{ $column['name'] }}_min-asc"><i class="fa fa-angle-up"></i></a>
						@else
						<i class="fa fa-angle-up"></i>
						@endif

						@if($order != $column["name"]."_min-desc")
						<a href="?{{ http_build_query(Input::except('order')) }}&order={{ $column['name'] }}_min-desc"><i class="fa fa-angle-down"></i></a>
						@else
						<i class="fa fa-angle-down"></i>
						@endif
					@endif
					{!! $column["title"] !!}
					@if($column["sortable"]) 
						@if($order != $column["name"]."_max-asc")
						<a href="?{{ http_build_query(Input::except('order')) }}&order={{ $column['name'] }}_max-asc"><i class="fa fa-angle-up"></i></a>
						@else
						<i class="fa fa-angle-up"></i>
						@endif

						@if($order != $column["name"]."_max-desc")
						<a href="?{{ http_build_query(Input::except('order')) }}&order={{ $column['name'] }}_max-desc"><i class="fa fa-angle-down"></i></a>
						@else
						<i class="fa fa-angle-down"></i>
						@endif
					@endif
				@else
					{!! $column["title"] !!}
					@if($column["sortable"]) 
						@if($order != $column["name"]."-asc")
						<a href="?{{ http_build_query(Input::except('order')) }}&order={{ $column['name'] }}-asc"><i class="fa fa-angle-up"></i></a>
						@else
						<i class="fa fa-angle-up"></i>
						@endif

						@if($order != $column["name"]."-desc")
						<a href="?{{ http_build_query(Input::except('order')) }}&order={{ $column['name'] }}-desc"><i class="fa fa-angle-down"></i></a>
						@else
						<i class="fa fa-angle-down"></i>
						@endif
					@endif
				@endif
			</th>
			@endforeach
			@if($any_actions)
			<th class="text-right">opcje</th>
			@endif
		</thead>
		<tbody>
			@foreach($data as $value)
			<tr>
				@if($allow_ordinal_column)
				<td>{{ $allow_ordinal_column++ }}</td>
				@endif
				@foreach($columns as $column)
				<td>
					@if(isset($column["boolean"]) && $column["boolean"])
						@if($column["value"]($value))
							<i class="fa fa-check-square-o"></i>
						@else 
							<i class="fa fa-square-o"></i>
						@endif
					@elseif(isset($column["minmax"]) && $column["minmax"])
						@if(is_null($column["value"]($value)["min"]) && is_null($column["value"]($value)["max"]))
							n/d
						@else
							{!! $column["value"]($value)["min"] !!} - {!! $column["value"]($value)["max"] !!}
						@endif
					@else
					{!! $column["value"]($value) !!}
					@endif
				</td>
				@endforeach
				@if($any_actions)
					<td class="text-right">
						@if($allowed_actions["edit"])
						<div class="btn-group open">
							<a class="btn btn-sm btn-info" href="{{ route('dashboard.'. $route .'.edit', $value->id) }}"><i class="fa fa-pencil fa-lg"></i></a>
						</div>
						@endif
						@if($allowed_actions["delete"])
						<div class="btn-group open">
							<a class="btn btn-sm btn-danger" href="{{ route('dashboard.'. $route .'.delete', $value->id) }}"><i class="fa fa-close fa-lg"></i></a>
						</div>
						@endif
						@if($allowed_actions["preview"])
						<div class="btn-group open">
							<a class="btn btn-sm btn-success" href="{{ route('dashboard.'. $route .'.view', $value->id) }}"><i class="fa fa-eye fa-lg"></i></a>
						</div>
						@endif
					</td>
				@endif
			</tr>
			@endforeach
		</tbody>
	</table>

@endsection