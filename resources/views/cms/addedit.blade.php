@extends("dashboard.root")

@section("dashboard.container")

	<h1>@if($object->id) Edytujesz @else Dodajesz @endif {{ $item_name }}</h1>

	<form class="col-sm-10 col-sm-offset-1" method="POST">
		<input type="hidden" name="_token" value="{!! csrf_token() !!}">

		@foreach($fields as $field)
			<div class="form-input-row form-group">
				<label class="col-sm-2 small">{{ $field["label"] }}</label>
				<div class="col-sm-10">

					@if($field["type"] == "select")
						<select name="{{ $field["name"] }}" class="form-control">
							@if(isset($field["default_none"]) && $field["default_none"])
							<option value="">brak</option>
							@endif
							@foreach($field["options"] as $option)
							<option value="{{ $option->id }}" @if((old($field["name"]) ?: $field["value"]($object)) == $option->id) selected="selected" @endif data-min-blg="{{ $option->og_min_plato }}" data-max-blg="{{ $option->og_max_plato }}">{{ $option->name }}</option>
							@endforeach
						</select>
					@else
						<input name="{{ $field["name"] }}" type="text" class="form-control" placeholder="{{ $field["label"] }}" value="{{ old($field["name"]) ?: $field["value"]($object) }}" autocomplete="off">
					@endif

				</div>
			</div>
		@endforeach

		<div class="submit-add-edit btn-group open pull-right">
			<button type="submit" class="btn btn-success btn-lg">zapisz</button>
			<button type="submit" class="btn btn-success btn-lg"><i class="fa fa-chevron-right"></i></button>
		</div>
	</form>

@endsection