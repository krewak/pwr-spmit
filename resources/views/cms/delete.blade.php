@extends("dashboard.root")

@section("dashboard.container")

	<h1>Usuwasz {{ $item_name }}: {{ $object->name }}</h1>

	<form class="col-sm-10 col-sm-offset-1" method="POST">
		<div class="alert alert-danger dashboard-alert">
			Usunięcie obiektu jest nieodwracalne. Musisz potwierdzić tę akcję.
		</div>

		<input type="hidden" name="_token" value="{!! csrf_token() !!}">

		<center>
			<div class="btn-group open ">
				<button type="submit" class="btn btn-danger btn-lg">usuń</button>
				<button type="submit" class="btn btn-danger btn-lg"><i class="fa fa-close"></i></button>
			</div>
		</center>
	</form>

@endsection